import os
import spdsxutils
import xml.etree.ElementTree as ET
import wavesamples

def list_files(directory):
    files = []
    for file_name in sorted(os.listdir(directory)):
        file_path = os.path.join(directory, file_name)
        if os.path.isfile(file_path):
            files.append(file_path)
    return files


def get_kit_name(file_path):
    kitname = spdsxutils.get_value(file_path,'Nm')
    kitSubname = spdsxutils.get_value(file_path,'SubNm')
    fullKitName = kitname + " - " + kitSubname
    if kitSubname == "":
        fullKitName = kitname
    return fullKitName

# ...

def set_name(file_path, nameArray):
    # Parse the XML file
    if len(nameArray) > 16:
        print(f'[KIT MANAGER] [SET_NAME] Name too long. Maximum 16 characters.')
        return False

    tree = ET.parse(file_path)
    root = tree.getroot()
    print(f"[KIT MANAGER] [SET_NAME] root: {root.tag}")

    # Create new Nm[0-9]{1,2} nodes with the new name
    counter = 0
    for nameChar in nameArray:
        counter_str = str(counter)
        if root.find("Nm" + counter_str) is not None:
            root.find("Nm" + counter_str).text = str(nameChar)  
        else:
            ET.SubElement(root, "Nm" + counter_str).text = str(nameChar)
        counter += 1
    for i in range(counter, 16):
        counter_str = str(i)
        if root.find("Nm" + counter_str) is not None:
            root.find("Nm" + counter_str).text = "0"       
        else:
            ET.SubElement(root, "Nm" + counter_str).text = "0"
    # Write the XML file
    tree.write(file_path, encoding="UTF-8", xml_declaration=True)
    return True

def get_volume(file_path):
    # Parse the XML file
    tree = ET.parse(file_path)
    root = tree.getroot()

    # Find the Volume node
    volume = root.find(".//Level")
    volume_value = int(volume.text)
    return volume_value

def set_volume(file_path, volume_value):
    # Parse the XML file
    tree = ET.parse(file_path)
    root = tree.getroot()

    # Find the Volume node
    volume = root.find(".//Level")
    volume.text = str(int(volume_value))

    # Write the XML file
    tree.write(file_path, encoding="UTF-8", xml_declaration=True)

def get_tempo(file_path):
    # Parse the XML file
    tree = ET.parse(file_path)
    root = tree.getroot()

    # Find the Tempo node
    tempo = root.find(".//Tempo")
    tempo_value = int(tempo.text)
    tempo_value /= 10
    return tempo_value

def set_tempo(file_path, tempo_value):
    # Parse the XML file
    tree = ET.parse(file_path)
    root = tree.getroot()

    # Find the Tempo node
    tempo = root.find(".//Tempo")
    tempo.text = str(int(tempo_value * 10))

    # Write the XML file
    tree.write(file_path, encoding="UTF-8", xml_declaration=True)

def get_pad_params(file_path):
    # Parse the XML file
    tree = ET.parse(file_path)
    root = tree.getroot()

    # Find all PadPrms/Wv and PadPrms/SubWv nodes
    pad_prms = root.findall(".//PadPrm")
    paramlist = []
    
    # Iterate through each PadPrm and get the Wv and SubWv subnodes
    i=0
    for pad_prm in pad_prms:
        param = {}
        i += 1
        param["Pad"]=str(i)
        param["Wave"]=pad_prm.find("Wv").text
        waveParamFile = wavesamples.get_wave_file(int(pad_prm.find("Wv").text))
        if waveParamFile is not None:
            param["WavePath"]=wavesamples.get_wave_audio_file(waveParamFile)
            param["WaveName"]=wavesamples.get_wave_name(waveParamFile)
        else:
            param["WavePath"]=param["WaveName"]="unset"
        subWaveParamFile = wavesamples.get_wave_file(int(pad_prm.find("SubWv").text))
        if subWaveParamFile is not None:
            param["SubWavePath"]=wavesamples.get_wave_audio_file(subWaveParamFile)
            param["SubWaveName"]=wavesamples.get_wave_name(subWaveParamFile)
        else:
            param["SubWavePath"]=param["SubWaveName"]="unset"
        paramlist.append(param)  
        
    return paramlist



def list_kits(input_directory):
    
    files = list_files(input_directory+'SPD-SX/KIT/')

    # Display all file names
    for i, file in enumerate(files):
        kitname = get_kit_name(file)
        print(f"[{i}] {os.path.basename(file)} - {kitname}")

    # Prompt for a number between 0 and the number of files
    num_files = len(files)
    user_input = input(f"\n ==============================================\nChoose a kit between 0 and {num_files - 1}: ")
    selected_file_index = int(user_input)

    # Check if the selected file index is valid
    if 0 <= selected_file_index < num_files:
        selected_file = files[selected_file_index]
        print(f"[KIT MANAGER] [LIST] You selected: {os.path.basename(selected_file)} - {get_kit_name(selected_file)}\n Tempo: {get_tempo(selected_file)}BPM // Volume: {get_volume(selected_file)}%")

        print(f'\n ==============================================\n')
        kitChoice = input(f'What would you like to do with this kit? M => Modify, L => List pads => D => Delete, C => Cancel ')
        if kitChoice == "M" or kitChoice == "m":
            print(f'[KIT MANAGER] [MODIFY] You chose to modify the kit.')
            newKitName = input(f'Enter the new kit name: (type enter to leave it unchanged)')
            kitNameChanged = True
            if newKitName == "":
                kitNameChanged = False
            else:
                nameArray = spdsxutils.get_ascii_values(newKitName)
                if set_name(selected_file,nameArray):
                    print(f'[KIT MANAGER] [MODIFY] Kit name changed to {newKitName}')
                else :
                    kitNameChanged = False
            if not kitNameChanged:
                print(f'[KIT MANAGER] [MODIFY] Kit name not changed.')
            newTempo = input(f'Enter the new tempo: (type enter to leave it unchanged)')
            tempoChanged = True
            if newTempo == "":
                tempoChanged = False
            elif not newTempo.isdigit():
                print(f'[KIT MANAGER] [MODIFY] Invalid tempo. Please enter a number.')
                tempoChanged = False
            else:
                newTempoInt = int(newTempo)
                set_tempo(selected_file,newTempoInt)
                print(f'[KIT MANAGER] [MODIFY] Tempo changed to {newTempo}')
            if not tempoChanged :
                print(f'[KIT MANAGER] [MODIFY] Tempo not changed.')
            newVolume = input(f'Enter the new volume: (type enter to leave it unchanged)')
            volumeChanged = True
            if newVolume == "":
                volumeChanged = False
            elif not newVolume.isdigit():
                print(f'[KIT MANAGER] [MODIFY] Invalid volume. Please enter a number.')
                volumeChanged = False
            else:
                newVolumeInt = int(newVolume)
                set_volume(selected_file,newVolumeInt)
                print(f'[KIT MANAGER] [MODIFY] Volume changed to {newVolume}')
            if not volumeChanged :
                print(f'[KIT MANAGER] [MODIFY] Volume not changed.')
        elif kitChoice == "D" or kitChoice == "d" :  
            print(f'[KIT MANAGER] [DELETE] You chose to delete the kit.')
            os.remove(selected_file)
            print(f'[KIT MANAGER] [DELETE] Kit deleted.')
        elif kitChoice == "C" or kitChoice == "c":  
            print(f'[KIT MANAGER] [CANCEL] You chose to cancel.')   
        elif kitChoice == "L" or kitChoice == "l":
            print(f'[KIT MANAGER] [PADS] You chose to list the pads.')
            padParams = get_pad_params(selected_file)
            for pad in padParams:
                print(f'Pad {pad["Pad"]} - Wave: {pad["WaveName"]} : {pad["WavePath"]} - SubWave: {pad["SubWaveName"]} : {pad["SubWavePath"]}')
    else:
        print("[KIT MANAGER] [LIST] Invalid input. Please enter a valid number.")


