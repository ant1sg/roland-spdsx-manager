import os
import spdsxutils
import xml.etree.ElementTree as ET

basewavepath = 'input/SPD-SX/WAVE/'

def list_wave_param_files():
    directory = os.path.join(basewavepath, 'PRM/')
    # Get a list of all subdirectories in the directory and sort them
    subdirectories = sorted([subdir for subdir in os.listdir(directory) if os.path.isdir(os.path.join(directory, subdir))])

    # Iterate over the subdirectories
    for subdir in subdirectories:
        print(f"Subdirectory: {subdir}")
        if subdir.isdigit():
            subdir_path = os.path.join(directory, subdir)
            xml_files = sorted([file for file in os.listdir(subdir_path) if file.endswith('.spd')])
            for xml_file in xml_files:
                print(f"[+] {subdir} - {xml_file} : {spdsxutils.get_value(os.path.join(subdir_path, xml_file), 'Nm')}")
            print()

            import xml.etree.ElementTree as ET

def get_wave_audio_file(xmlfile):
    tree = ET.parse(xmlfile)
    root = tree.getroot()
    path_node = root.find('Path')
    if path_node is not None:
        return path_node.text
    else:
        return None

def get_wave_name(xmlfile):
    return spdsxutils.get_value(xmlfile, 'Nm')

def get_wave_file(waveId):
    directory = os.path.join(basewavepath, 'PRM/')
    subdirectories = sorted([subdir for subdir in os.listdir(directory) if os.path.isdir(os.path.join(directory, subdir))])
    count = 0   
    for subdir in subdirectories:
        if subdir.isdigit():
            subdir_path = os.path.join(directory, subdir)
            xml_files = sorted([file for file in os.listdir(subdir_path) if file.endswith('.spd')])
            for xml_file in xml_files:
                if waveId == count:
                    return os.path.join(subdir_path, xml_file )
                count += 1
