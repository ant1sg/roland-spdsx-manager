import kit as kit
import wavesamples as wave
import os
import tarfile


input_directory = 'input/'
archive_name = 'SPD-SX-BKUP-ALL.spd'
format = tarfile.GNU_FORMAT

def untar_spd_file(file_path):
    # Extract the directory path and file name
    directory = os.path.dirname(file_path)
    file_name = os.path.basename(file_path)

    # Open the tar archive file
    with tarfile.open(file_path, 'r') as tar:
        # Extract all files to the same directory
        tar.extractall(directory)

    print(f"[KIT MANAGER] [EXTRACT] Successfully untarred {file_name} in {directory}")


def create_tar_archive(output_directory):
    input_directory = 'input/'
    archive_name = 'SPD-SX-BKUP-ALL.spd'
    output_directory = 'output/'
    
    # Create the output directory if it doesn't exist
    os.makedirs(output_directory, exist_ok=True)
    
    print(f'[KIT MANAGER] [TAR] Creating tar archive {archive_name} in {output_directory} please wait...')
    for root, dirs, files in os.walk(input_directory):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            print(file_path)

            # Add the file to the tar archive
            archive_path = os.path.join(output_directory, archive_name)
            with tarfile.open(archive_path, 'a', format=format) as tar:
                tar.add(file_path, arcname=os.path.relpath(file_path, input_directory))

    print(f"[KIT MANAGER] [TAR] Successfully created tar archive {archive_name} in {output_directory}")
    




print("""
:'######::'########::'########::::::::::::'######::'##::::'##:
'##... ##: ##.... ##: ##.... ##::::::::::'##... ##:. ##::'##::
 ##:::..:: ##:::: ##: ##:::: ##:::::::::: ##:::..:::. ##'##:::
. ######:: ########:: ##:::: ##:'#######:. ######::::. ###::::
:..... ##: ##.....::: ##:::: ##:........::..... ##::: ## ##:::
'##::: ##: ##:::::::: ##:::: ##::::::::::'##::: ##:: ##:. ##::
. ######:: ##:::::::: ########:::::::::::. ######:: ##:::. ##:
:......:::..:::::::::........:::::::::::::......:::..:::::..::                                                  
""")
print("Welcome to the Roland SPD-SX Manager \nProgram by ASG DEV https://antoine.sirven-gabiache.fr\n\n")

choice = ''
while choice != 'q':
    choice = input("Please select an option : k => manage kits, w => manage wave files, x => extract archive, t => create new spd archive, q => quit): ")

    if choice == 'k':
        choice = input("[KIT MANAGER] Please select an option : l => list kits, c => create kit, d => delete kit, q => quit): ")
        if choice == 'l':
            kit.list_kits(input_directory)
        elif choice == 'c':
            createKitName = input("[KIT MANAGER] [CREATE] Please enter the name of the kit you want to create : ")
            print("Creating kit " + createKitName)
    elif choice == 'w':
        wave.list_wave_param_files()
    elif choice == 'x':
        extractconfirm = input("[KIT MANAGER] [EXTRACT] Are you sure you want to extract the archive ? This will overwrite any modification you made to local file (y/n) : ")   
        file_path = os.path.join(input_directory, archive_name)
        untar_spd_file(file_path)
    elif choice == 't':
        create_tar_archive(input_directory)
    elif choice == 'q':
        print("Exiting the program. Goodbye!")
    else:
        print("Invalid choice. Please try again.")
