import xml.etree.ElementTree as ET

def get_value(file_path, prefix):
    prefix_length = len(prefix)  # Get the length of the prefix

    # Parse the XML file
    tree = ET.parse(file_path)
    root = tree.getroot()

    # Initialize the concatenated content
    concatenated_content = ""

    # Iterate through all nodes starting with Nm[0-9]{1-2}
    for node in root.iter():
        if node.tag.startswith(prefix) and len(node.tag) in [prefix_length + 1,prefix_length + 2] and node.tag[prefix_length:].isdigit():
            concatenated_content += chr(int(node.text))

    return concatenated_content

def get_ascii_values(string):
    ascii_values = []
    for char in string:
        ascii_values.append(ord(char))
        print(f'[SPD-SX UTILS] [GET_ASCII_VALUES] {char} : {ord(char)}')
    return ascii_values
